[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/vitiuk-space/ubuntu)

# docker-ubuntu-base

## Docker images:

### Docker Hub: `313u/ubuntu:latest`

****

Slightly modified phusion/baseimage Ubuntu base image

- Includes `ping` and `tzdata` packages
- Container timezone is set via environment variable `${CONTAINER_TIMEZONE:-$DEFAULT_CONTAINER_TIMEZONE}`, defaults to `Europe/Kiev`
- Modified apt sources.list to use `http://mirror.rackspace.com/ubuntu/`, which seems reliable, fast and local to most servers
